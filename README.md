# Alphabet Soup Implementation

## Directory structure
The following is a list of the directories and their purpose

* com.dsmoot - The root directory for the project. Contains the Main class
    * exception - Contains the exception class
    * finder - Contains the WordFinder class. This class find the specified word in the puzzle
    * searcher - Contains the Searcher classes. These classes implement the search vector when searching for a word in the puzzle
        * AbstractWordSearcher - This class implements the base logic to search for a word
        * DiagonalBottomLeftToTopRight - This class implements the logic to search for a word based on the vector defined by the class name
        * DiagonalTopRightToBottomLeft - This class implements the logic to search for a word based on the vector defined by the class name
        * DiagonalBottomRightToTopLeft - This class implements the logic to search for a word based on the vector defined by the class name
        * DiagonalTopLeftToBottomRight - This class implements the logic to search for a word based on the vector defined by the class name
        * HorizontalLeftToRight - This class implements the logic to search for a word based on the vector defined by the class name
        * HorizontalRightToLeft - This class implements the logic to search for a word based on the vector defined by the class name
        * VerticalTopToBottom - This class implements the logic to search for a word based on the vector defined by the class name
        * VerticalBottomToTop - This class implements the logic to search for a word based on the vector defined by the class name
    * solver - Contains the puzzle solver classes
        * OldPuzzleSolver - This class contains the original implementation for the puzzle solver. I left this class to use as a base line for comparing the new implementation.
        * NewPuzzleSolver - This class contains the new implementation for the puzzle solver. I re-factored the implementation to use the WordFinder and Searcher classes.

###### Synchronous vs Asynchronous
> The Old Puzzle solver class can solve the puzzle by finding a word one after the other. When using the searchers, each searcher is executed one after the other, The the search fails, the the next search vector is executed. In asynchronous mode, the searchers are executed on parallel. The 1st searcher that finds the word, will set a mutex letting the other searcher know that the word has been found and that they can stop searching.

> The New Puzzle solver class can solve the puzzle by finding a word one after the other. When using the searchers, each searcher is executed one after the other, The the search fails, the the next search vector is executed. In asynchronous mode, all of the words are searched in parallel. The searchers are also executed on parallel. Once all of the words has been found, then the output is generated.


## Compiling Application and Generating Executable JAR file
I use Maven to compile and generate the JAR file. During the compilation phase I use the spotbugs-maven-plugin to check for coding issues.
               

## Testing Application
The following describes the test data and classes that were used for testing the application

### Test Data
Located in the src/main/resources folder are sample files that I used for testing. Located in the src/test/resources

### JUnit Testing
Located in the src/test/java are the JUnit test classes
Located in the src/test/resources are the input files used by the JUnit test classes
Located in the src/test/resources is the WordPuzzle.xlsx file that I used to create the input50x50.txt file for testing purposes.

### Running Application Test
Located in the root of the project is the RunTest.bat file. This batch file, assuming that you are using Windows, is used to execute the application. There are 4 test cases
* Test Cases
    * old - The /old parameter uses the original (OldPuzzleSolver) implementation to solve the puzzle
    * old async  - The /old /async parameters uses the original asynchronous (OldPuzzleSolver) implementation to solve the puzzle
    * new - With no parameters specified the application uses the new (NewPuzzleSolver) implementation to solve the puzzle
    * new async - The /async parameter uses the new asynchronous (NewPuzzleSolver) implementation to solve the puzzle
    