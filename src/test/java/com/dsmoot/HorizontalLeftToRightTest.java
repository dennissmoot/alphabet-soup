package com.dsmoot;

import static org.junit.jupiter.api.Assertions.*;

import java.io.IOException;

import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.dsmoot.exception.AlphabetSoupException;
import com.dsmoot.solver.OldPuzzleSolver;

class HorizontalLeftToRightTest {
	private static final Logger log = LoggerFactory.getLogger(HorizontalLeftToRightTest.class);

	@Test
	void test() {
		try {
			OldPuzzleSolver a = new OldPuzzleSolver("src/test/resources/input-horizontal-left-right.txt");
			String response = a.findWordHorizontalLeftToRight("ABC".toCharArray());
			if (log.isDebugEnabled()) log.debug(response);
			assertEquals("0:0 0:2", response);
			
			response = a.findWordHorizontalLeftToRight("DEF".toCharArray());
			if (log.isDebugEnabled()) log.debug(response);
			assertEquals("1:1 1:3", response);
			
			response = a.findWordHorizontalLeftToRight("HI".toCharArray());
			if (log.isDebugEnabled()) log.debug(response);
			assertEquals("2:3 2:4", response);
			
			response = a.findWordHorizontalLeftToRight("OETB".toCharArray());
			if (log.isDebugEnabled()) log.debug(response);
			assertEquals("3:1 3:4", response);
			
			response = a.findWordHorizontalLeftToRight("YOBVS".toCharArray());
			if (log.isDebugEnabled()) log.debug(response);
			assertEquals("4:0 4:4", response);
			
		} catch (AlphabetSoupException e) {
			fail(e.getMessage());
		} catch (IOException e) {
			fail(e.getMessage());
		}
		
	}

}
