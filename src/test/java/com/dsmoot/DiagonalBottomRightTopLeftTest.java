package com.dsmoot;

import static org.junit.jupiter.api.Assertions.*;

import java.io.IOException;

import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.dsmoot.exception.AlphabetSoupException;
import com.dsmoot.solver.OldPuzzleSolver;

class DiagonalBottomRightTopLeftTest {
	private static final Logger log = LoggerFactory.getLogger(DiagonalBottomRightTopLeftTest.class);

	@Test
	void test() {
		try {
			OldPuzzleSolver a = new OldPuzzleSolver("src/test/resources/input-diagonal-top-left-bottom-right.txt");
			String response = a.findWordDiagonalBottomRightToTopLeft("STGDA".toCharArray());
			if (log.isDebugEnabled()) log.debug(response);
			assertEquals("4:4 0:0", response);
			
			response = a.findWordDiagonalBottomRightToTopLeft("BOS".toCharArray());
			if (log.isDebugEnabled()) log.debug(response);
			assertEquals("4:2 2:0", response);
			
			response = a.findWordDiagonalBottomRightToTopLeft("BHE".toCharArray());
			if (log.isDebugEnabled()) log.debug(response);
			assertEquals("3:4 1:2", response);
			
			response = a.findWordDiagonalBottomRightToTopLeft("EO".toCharArray());
			if (log.isDebugEnabled()) log.debug(response);
			assertEquals("1:4 0:3", response);
		} catch (AlphabetSoupException e) {
			fail(e.getMessage());
		} catch (IOException e) {
			fail(e.getMessage());
		}
	}
}
