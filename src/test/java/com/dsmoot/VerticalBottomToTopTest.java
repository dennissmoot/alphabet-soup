package com.dsmoot;

import static org.junit.jupiter.api.Assertions.*;

import java.io.IOException;

import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.dsmoot.exception.AlphabetSoupException;
import com.dsmoot.solver.OldPuzzleSolver;

class VerticalBottomToTopTest {
	private static final Logger log = LoggerFactory.getLogger(VerticalBottomToTopTest.class);

	@Test
	void test() {
		try {
			OldPuzzleSolver a = new OldPuzzleSolver("src/test/resources/input-vertical-bottom-top.txt");
			String response = a.findWordVerticalBottomToTop("ABC".toCharArray());
			if (log.isDebugEnabled()) log.debug(response);
			assertEquals("2:0 0:0", response);
			
			response = a.findWordVerticalBottomToTop("DEF".toCharArray());
			if (log.isDebugEnabled()) log.debug(response);
			assertEquals("3:1 1:1", response);
			
			response = a.findWordVerticalBottomToTop("HI".toCharArray());
			if (log.isDebugEnabled()) log.debug(response);
			assertEquals("3:4 2:4", response);

			response = a.findWordVerticalBottomToTop("DEFB".toCharArray());
			if (log.isDebugEnabled()) log.debug(response);
			assertEquals("3:1 0:1", response);

			response = a.findWordVerticalBottomToTop("BXABC".toCharArray());
			if (log.isDebugEnabled()) log.debug(response);
			assertEquals("4:0 0:0", response);
		} catch (AlphabetSoupException e) {
			fail(e.getMessage());
		} catch (IOException e) {
			fail(e.getMessage());
		}
	}
}
