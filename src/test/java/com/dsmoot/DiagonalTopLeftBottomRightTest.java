package com.dsmoot;

import static org.junit.jupiter.api.Assertions.*;

import java.io.IOException;

import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.dsmoot.exception.AlphabetSoupException;
import com.dsmoot.solver.OldPuzzleSolver;

class DiagonalTopLeftBottomRightTest {
	private static final Logger log = LoggerFactory.getLogger(DiagonalTopLeftBottomRightTest.class);

	@Test
	void test() {
		try {
			OldPuzzleSolver a = new OldPuzzleSolver("src/test/resources/input-diagonal-top-left-bottom-right.txt");
			String response = a.findWordDiagonalTopLeftToBottomRight("ADGTS".toCharArray());
			if (log.isDebugEnabled()) log.debug(response);
			assertEquals("0:0 4:4", response);
			
			response = a.findWordDiagonalTopLeftToBottomRight("SOB".toCharArray());
			if (log.isDebugEnabled()) log.debug(response);
			assertEquals("2:0 4:2", response);
			
			response = a.findWordDiagonalTopLeftToBottomRight("DGT".toCharArray());
			if (log.isDebugEnabled()) log.debug(response);
			assertEquals("1:1 3:3", response);
			
			response = a.findWordDiagonalTopLeftToBottomRight("OE".toCharArray());
			if (log.isDebugEnabled()) log.debug(response);
			assertEquals("0:3 1:4", response);
		} catch (AlphabetSoupException e) {
			fail(e.getMessage());
		} catch (IOException e) {
			fail(e.getMessage());
		}
	}
}
