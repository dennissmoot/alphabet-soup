package com.dsmoot;

import static org.junit.jupiter.api.Assertions.*;

import java.io.IOException;

import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.dsmoot.exception.AlphabetSoupException;
import com.dsmoot.solver.NewPuzzleSolver;

class NewAlphabetSoupTest {
	private static final Logger log = LoggerFactory.getLogger(NewAlphabetSoupTest.class);

	@Test
	void findWordHorizontalLeftToRight() {
		try {
			NewPuzzleSolver a = new NewPuzzleSolver("src/test/resources/input-horizontal-left-right.txt");
			String response = a.findWordHorizontalLeftToRight("ABC".toCharArray());
			if (log.isDebugEnabled()) log.debug(response);
			assertEquals("0:0 0:2", response);
			
			response = a.findWordHorizontalLeftToRight("DEF".toCharArray());
			if (log.isDebugEnabled()) log.debug(response);
			assertEquals("1:1 1:3", response);
			
			response = a.findWordHorizontalLeftToRight("HI".toCharArray());
			if (log.isDebugEnabled()) log.debug(response);
			assertEquals("2:3 2:4", response);
			
			response = a.findWordHorizontalLeftToRight("OETB".toCharArray());
			if (log.isDebugEnabled()) log.debug(response);
			assertEquals("3:1 3:4", response);
			
			response = a.findWordHorizontalLeftToRight("YOBVS".toCharArray());
			if (log.isDebugEnabled()) log.debug(response);
			assertEquals("4:0 4:4", response);
		} catch (AlphabetSoupException e) {
			fail(e.getMessage());
		} catch (IOException e) {
			fail(e.getMessage());
		}
	}

	@Test
	void findWordHorizontalRightToLeft() {
		try {
			NewPuzzleSolver a = new NewPuzzleSolver("src/test/resources/input-horizontal-right-left.txt");
			String response = a.findWordHorizontalRightToLeft("ABC".toCharArray());
			if (log.isDebugEnabled()) log.debug(response);
			assertEquals("0:2 0:0", response);
			
			response = a.findWordHorizontalRightToLeft("DEF".toCharArray());
			if (log.isDebugEnabled()) log.debug(response);
			assertEquals("1:3 1:1", response);
			
			response = a.findWordHorizontalRightToLeft("HI".toCharArray());
			if (log.isDebugEnabled()) log.debug(response);
			assertEquals("2:4 2:3", response);

			response = a.findWordHorizontalRightToLeft("TEOY".toCharArray());
			if (log.isDebugEnabled()) log.debug(response);
			assertEquals("3:3 3:0", response);

			response = a.findWordHorizontalRightToLeft("SVBOY".toCharArray());
			if (log.isDebugEnabled()) log.debug(response);
			assertEquals("4:4 4:0", response);
		} catch (AlphabetSoupException e) {
			fail(e.getMessage());
		} catch (IOException e) {
			fail(e.getMessage());
		}
	}
	
	@Test
	void findWordVerticalTopToBottom() {
		try {
			NewPuzzleSolver a = new NewPuzzleSolver("src/test/resources/input-vertical-top-bottom.txt");
			String response = a.findWordVerticalTopToBottom("ABC".toCharArray());
			if (log.isDebugEnabled()) log.debug(response);
			assertEquals("0:0 2:0", response);
			
			response = a.findWordVerticalTopToBottom("DEF".toCharArray());
			if (log.isDebugEnabled()) log.debug(response);
			assertEquals("1:1 3:1", response);
			
			response = a.findWordVerticalTopToBottom("DISD".toCharArray());
			if (log.isDebugEnabled()) log.debug(response);
			assertEquals("1:3 4:3", response);

			response = a.findWordVerticalTopToBottom("REGHI".toCharArray());
			if (log.isDebugEnabled()) log.debug(response);
			assertEquals("0:4 4:4", response);
		} catch (AlphabetSoupException e) {
			fail(e.getMessage());
		} catch (IOException e) {
			fail(e.getMessage());
		}
	}

	@Test
	void findWordVerticalBottomToTop() {
		try {
			NewPuzzleSolver a = new NewPuzzleSolver("src/test/resources/input-vertical-bottom-top.txt");
			String response = a.findWordVerticalBottomToTop("ABC".toCharArray());
			if (log.isDebugEnabled()) log.debug(response);
			assertEquals("2:0 0:0", response);
			
			response = a.findWordVerticalBottomToTop("DEF".toCharArray());
			if (log.isDebugEnabled()) log.debug(response);
			assertEquals("3:1 1:1", response);
			
			response = a.findWordVerticalBottomToTop("HI".toCharArray());
			if (log.isDebugEnabled()) log.debug(response);
			assertEquals("3:4 2:4", response);

			response = a.findWordVerticalBottomToTop("DEFB".toCharArray());
			if (log.isDebugEnabled()) log.debug(response);
			assertEquals("3:1 0:1", response);

			response = a.findWordVerticalBottomToTop("BXABC".toCharArray());
			if (log.isDebugEnabled()) log.debug(response);
			assertEquals("4:0 0:0", response);
		} catch (AlphabetSoupException e) {
			fail(e.getMessage());
		} catch (IOException e) {
			fail(e.getMessage());
		}
	}

	@Test
	void findWordDiagonalTopLeftToBottomRight() {
		try {
			NewPuzzleSolver a = new NewPuzzleSolver("src/test/resources/input-diagonal-top-left-bottom-right.txt");
			String response = a.findWordDiagonalTopLeftToBottomRight("ADGTS".toCharArray());
			if (log.isDebugEnabled()) log.debug(response);
			assertEquals("0:0 4:4", response);
			
			response = a.findWordDiagonalTopLeftToBottomRight("SOB".toCharArray());
			if (log.isDebugEnabled()) log.debug(response);
			assertEquals("2:0 4:2", response);
			
			response = a.findWordDiagonalTopLeftToBottomRight("DGT".toCharArray());
			if (log.isDebugEnabled()) log.debug(response);
			assertEquals("1:1 3:3", response);
			
			response = a.findWordDiagonalTopLeftToBottomRight("OE".toCharArray());
			if (log.isDebugEnabled()) log.debug(response);
			assertEquals("0:3 1:4", response);
		} catch (AlphabetSoupException e) {
			fail(e.getMessage());
		} catch (IOException e) {
			fail(e.getMessage());
		}
	}

	@Test
	void findWordDiagonalBottomRightToTopLeft() {
		try {
			NewPuzzleSolver a = new NewPuzzleSolver("src/test/resources/input-diagonal-top-left-bottom-right.txt");
			String response = a.findWordDiagonalBottomRightToTopLeft("STGDA".toCharArray());
			if (log.isDebugEnabled()) log.debug(response);
			assertEquals("4:4 0:0", response);
			
			response = a.findWordDiagonalBottomRightToTopLeft("BOS".toCharArray());
			if (log.isDebugEnabled()) log.debug(response);
			assertEquals("4:2 2:0", response);
			
			response = a.findWordDiagonalBottomRightToTopLeft("BHE".toCharArray());
			if (log.isDebugEnabled()) log.debug(response);
			assertEquals("3:4 1:2", response);
			
			response = a.findWordDiagonalBottomRightToTopLeft("EO".toCharArray());
			if (log.isDebugEnabled()) log.debug(response);
			assertEquals("1:4 0:3", response);
		} catch (AlphabetSoupException e) {
			fail(e.getMessage());
		} catch (IOException e) {
			fail(e.getMessage());
		}
	}

	@Test
	void findWordDiagonalBottomLeftToTopRight() {
		try {
			NewPuzzleSolver a = new NewPuzzleSolver("src/test/resources/input-diagonal-bottom-left-top-right.txt");
			String response = a.findWordDiagonalBottomLeftToTopRight("YOGFR".toCharArray());
			if (log.isDebugEnabled()) log.debug(response);
			assertEquals("4:0 0:4", response);
			
			response = a.findWordDiagonalBottomLeftToTopRight("SDC".toCharArray());
			if (log.isDebugEnabled()) log.debug(response);
			assertEquals("2:0 0:2", response);
			
			response = a.findWordDiagonalBottomLeftToTopRight("BTI".toCharArray());
			if (log.isDebugEnabled()) log.debug(response);
			assertEquals("4:2 2:4", response);
			
			response = a.findWordDiagonalBottomLeftToTopRight("VB".toCharArray());
			if (log.isDebugEnabled()) log.debug(response);
			assertEquals("4:3 3:4", response);
		} catch (AlphabetSoupException e) {
			fail(e.getMessage());
		} catch (IOException e) {
			fail(e.getMessage());
		}
	}

	@Test
	void findWordDiagonalTopRightToBottomLeft() {
		try {
			NewPuzzleSolver a = new NewPuzzleSolver("src/test/resources/input-diagonal-top-right-bottom-left.txt");
			String response = a.findWordDiagonalTopRightToBottomLeft("RFGOY".toCharArray());
			if (log.isDebugEnabled()) log.debug(response);
			assertEquals("0:4 4:0", response);
			
			response = a.findWordDiagonalTopRightToBottomLeft("ITB".toCharArray());
			if (log.isDebugEnabled()) log.debug(response);
			assertEquals("2:4 4:2", response);
			
			response = a.findWordDiagonalTopRightToBottomLeft("ECY".toCharArray());
			if (log.isDebugEnabled()) log.debug(response);
			assertEquals("1:2 3:0", response);
			
			response = a.findWordDiagonalTopRightToBottomLeft("BV".toCharArray());
			if (log.isDebugEnabled()) log.debug(response);
			assertEquals("3:4 4:3", response);
		} catch (AlphabetSoupException e) {
			fail(e.getMessage());
		} catch (IOException e) {
			fail(e.getMessage());
		}
	}
}
