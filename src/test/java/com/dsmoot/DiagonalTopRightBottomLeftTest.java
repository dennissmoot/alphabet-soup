package com.dsmoot;

import static org.junit.jupiter.api.Assertions.*;

import java.io.IOException;

import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.dsmoot.exception.AlphabetSoupException;
import com.dsmoot.solver.OldPuzzleSolver;

class DiagonalTopRightBottomLeftTest {
	private static final Logger log = LoggerFactory.getLogger(DiagonalTopRightBottomLeftTest.class);

	@Test
	void test() {
		try {
			OldPuzzleSolver a = new OldPuzzleSolver("src/test/resources/input-diagonal-top-right-bottom-left.txt");
			String response = a.findWordDiagonalTopRightToBottomLeft("RFGOY".toCharArray());
			if (log.isDebugEnabled()) log.debug(response);
			assertEquals("0:4 4:0", response);
			
			response = a.findWordDiagonalTopRightToBottomLeft("ITB".toCharArray());
			if (log.isDebugEnabled()) log.debug(response);
			assertEquals("2:4 4:2", response);
			
			response = a.findWordDiagonalTopRightToBottomLeft("ECY".toCharArray());
			if (log.isDebugEnabled()) log.debug(response);
			assertEquals("1:2 3:0", response);
			
			response = a.findWordDiagonalTopRightToBottomLeft("BV".toCharArray());
			if (log.isDebugEnabled()) log.debug(response);
			assertEquals("3:4 4:3", response);
		} catch (AlphabetSoupException e) {
			fail(e.getMessage());
		} catch (IOException e) {
			fail(e.getMessage());
		}
	}
}
