package com.dsmoot;

import static org.junit.jupiter.api.Assertions.*;

import java.io.IOException;

import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.dsmoot.exception.AlphabetSoupException;
import com.dsmoot.solver.OldPuzzleSolver;

class HorizontalRightToLeftTest {
	private static final Logger log = LoggerFactory.getLogger(HorizontalRightToLeftTest.class);

	@Test
	void test() {
		try {
			OldPuzzleSolver a = new OldPuzzleSolver("src/test/resources/input-horizontal-right-left.txt");
			String response = a.findWordHorizontalRightToLeft("ABC".toCharArray());
			if (log.isDebugEnabled()) log.debug(response);
			assertEquals("0:2 0:0", response);
			
			response = a.findWordHorizontalRightToLeft("DEF".toCharArray());
			if (log.isDebugEnabled()) log.debug(response);
			assertEquals("1:3 1:1", response);
			
			response = a.findWordHorizontalRightToLeft("HI".toCharArray());
			if (log.isDebugEnabled()) log.debug(response);
			assertEquals("2:4 2:3", response);

			response = a.findWordHorizontalRightToLeft("TEOY".toCharArray());
			if (log.isDebugEnabled()) log.debug(response);
			assertEquals("3:3 3:0", response);

			response = a.findWordHorizontalRightToLeft("SVBOY".toCharArray());
			if (log.isDebugEnabled()) log.debug(response);
			assertEquals("4:4 4:0", response);
		} catch (AlphabetSoupException e) {
			fail(e.getMessage());
		} catch (IOException e) {
			fail(e.getMessage());
		}
	}
}
