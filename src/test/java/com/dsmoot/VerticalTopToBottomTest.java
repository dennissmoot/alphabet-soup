package com.dsmoot;

import static org.junit.jupiter.api.Assertions.*;

import java.io.IOException;

import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.dsmoot.exception.AlphabetSoupException;
import com.dsmoot.solver.OldPuzzleSolver;

class VerticalTopToBottomTest {
	private static final Logger log = LoggerFactory.getLogger(VerticalTopToBottomTest.class);

	@Test
	void test() {
		try {
			OldPuzzleSolver a = new OldPuzzleSolver("src/test/resources/input-vertical-top-bottom.txt");
			String response = a.findWordVerticalTopToBottom("ABC".toCharArray());
			if (log.isDebugEnabled()) log.debug(response);
			assertEquals("0:0 2:0", response);
			
			response = a.findWordVerticalTopToBottom("DEF".toCharArray());
			if (log.isDebugEnabled()) log.debug(response);
			assertEquals("1:1 3:1", response);
			
			response = a.findWordVerticalTopToBottom("DISD".toCharArray());
			if (log.isDebugEnabled()) log.debug(response);
			assertEquals("1:3 4:3", response);

			response = a.findWordVerticalTopToBottom("REGHI".toCharArray());
			if (log.isDebugEnabled()) log.debug(response);
			assertEquals("0:4 4:4", response);
		} catch (AlphabetSoupException e) {
			fail(e.getMessage());
		} catch (IOException e) {
			fail(e.getMessage());
		}
	}
}
