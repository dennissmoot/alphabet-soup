package com.dsmoot.searcher;

import java.util.concurrent.CompletableFuture;

public interface Searcher {
	public String search(char[][]puzzle, int puzzleWidth, int puzzleHeight, char[] wordToFind, int wordLength);

    public CompletableFuture<String> searchAsync(char[][]puzzle, int puzzleWidth, int puzzleHeight, char[] wordToFind, int wordLength);
}
