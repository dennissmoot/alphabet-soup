package com.dsmoot.searcher;

import java.time.Duration;
import java.time.Instant;
import java.util.concurrent.CompletableFuture;
import java.util.function.Supplier;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.dsmoot.Main;
import com.dsmoot.finder.Finder;
import com.dsmoot.solver.OldPuzzleSolver;
import com.dsmoot.util.DurationUtil;

@SuppressWarnings("unused")
public abstract class AbstractWordSearcher implements Searcher {
    private static final Logger log = LoggerFactory.getLogger(AbstractWordSearcher.class);

    protected int startRow = 0;
    protected int startCol = 0;
    // the lastRow and lastCol will contain values that outside the boundary for searching for words
    protected int lastRow = 0;
    protected int lastCol = 0;
    protected int rowIncrementer = 0;
    protected int colIncrementer = 0;
    protected int sRow = -1, sCol = -1, eRow = -1, eCol = -1;
    protected boolean foundIntable = false;
    //protected char w, t;
    protected int row = 0;
    protected int col = 0;
    protected char[][]puzzle;

    protected abstract int getPuzzleRow(int c);
    protected abstract int getPuzzleCol(int c);
    protected abstract void initialize(char[][]puzzle, int puzzleWidth, int puzzleHeight, int wordLength);
    protected abstract boolean lettersMatch(char wordLetter, int tRow, int tCol);

    private Finder finder;
    
    public AbstractWordSearcher(Finder finder) {
        this.finder = finder;
    }
    
    public int getUpperBoundary(int tableSize, int wordSize, Direction direction) {
        if (direction == Direction.FORWARD) return tableSize - wordSize + 1;
        return wordSize - 2;
    }

    public String search(char[][]puzzle, int puzzleWidth, int puzzleHeight, char[] wordToFind, int wordLength) {
        Instant begin = null, end = null;
        if (Main.EnableDebug && log.isDebugEnabled()) begin = Instant.now();

        initialize(puzzle, puzzleWidth, puzzleHeight, wordLength);

        this.row = this.startRow;
        while (this.row != this.lastRow) {
            this.col = this.startCol;
            while (this.col != this.lastCol) {
                // check to see if word was found in another thread
                // placed where for now to improve performance reasons
                if (finder.isWordFound()) {
                    return "";
                }

                // search for word
                boolean found = true;
                // intialize tracking values
                this.sRow = this.sCol = this.eRow = this.eCol = -1;
                for (int c = 0; c < wordLength; c++) {
                    int tRow = getPuzzleRow(c), tCol = getPuzzleCol(c);
                    if (!lettersMatch(wordToFind[c], tRow, tCol)) {
                        found = false; // set flag that word not found
                        break;
                    }
                }
                // if word found then set flags to break out of loops
                if (found) {
                    foundIntable = true;
                    break;
                }
                this.col += this.colIncrementer;
            }

            if (this.foundIntable) break;
            this.row += this.rowIncrementer;
        }

        if (!this.foundIntable) {
            if (Main.EnableDebug && log.isDebugEnabled()) {
                end = Instant.now();
                Duration dur = Duration.between(begin, end);
                log.debug(String.format("Not Found - %s %s", this.getClass().getName(), DurationUtil.toSeconds(dur).toString()));
            }
            return "";
        }

        finder.setWordFound(true);
        if (Main.EnableDebug && log.isDebugEnabled()) {
            end = Instant.now();
            Duration dur = Duration.between(begin, end);
            log.debug(String.format("%d:%d %d:%d - %s %s", sRow, sCol, eRow, eCol, this.getClass().getName(), DurationUtil.toSeconds(dur).toString()));
        }

        return String.format("%d:%d %d:%d", sRow, sCol, eRow, eCol);
    }

    public CompletableFuture<String> searchAsync(char[][]puzzle, int puzzleWidth, int puzzleHeight, char[] wordToFind, int wordLength) {
        return CompletableFuture.supplyAsync(new Supplier<String>() {
            @Override
            public String get() {
                return search(puzzle, puzzleWidth, puzzleHeight, wordToFind, wordLength);
            }
        });
    }
}
