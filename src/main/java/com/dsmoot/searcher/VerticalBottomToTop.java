package com.dsmoot.searcher;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.dsmoot.Main;
import com.dsmoot.finder.Finder;

@SuppressWarnings("unused")
public class VerticalBottomToTop extends AbstractWordSearcher {
    private static final Logger log = LoggerFactory.getLogger(VerticalBottomToTop.class);

    public VerticalBottomToTop(Finder finder) {
        super(finder);
    }
    
    //int tRow = row - c, tCol = col;
    protected int getPuzzleRow(int c) {
        return this.row - c;
    }

    protected int getPuzzleCol(int c) {
        return this.col;
    }

    protected void initialize(char[][]puzzle, int puzzleWidth, int puzzleHeight, int wordLength) {
        this.puzzle = puzzle;
        this.foundIntable = false;
        this.startRow =  puzzleHeight - 1;
        this.startCol = 0;
        this.lastRow = getUpperBoundary(puzzleHeight, wordLength, Direction.BACKWARD);
        this.lastCol = puzzleWidth;
        this.rowIncrementer = -1;
        this.colIncrementer = 1;
        this.sRow = this.sCol = this.eRow = this.eCol = -1;
        //if (Main.EnableDebug && log.isDebugEnabled()) log.debug(String.format("startRow %d => lastRow %d  startCol %d => lastCol %d", startRow, lastRow - 1, startCol, lastCol - 1));
    }

    protected boolean lettersMatch(char wordLetter, int tRow, int tCol) {
        if (Main.EnableTrace && log.isTraceEnabled()) log.trace(String.format("Row %d  Col %d  %c  %c", tRow, tCol, puzzle[tRow][tCol], wordLetter));
        if (wordLetter != puzzle[tRow][tCol]) return false;

        if (this.sRow < 0) { // start tracking position of word
            this.sRow = tRow;
            this.sCol = this.eCol = tCol;
        }
        this.eRow = tRow; // save last position that was successful

        return true;
    }
}
