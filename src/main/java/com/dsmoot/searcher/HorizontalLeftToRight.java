package com.dsmoot.searcher;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.dsmoot.Main;
import com.dsmoot.finder.Finder;

@SuppressWarnings("unused")
public class HorizontalLeftToRight extends AbstractWordSearcher {
    private static final Logger log = LoggerFactory.getLogger(HorizontalLeftToRight.class);

    public HorizontalLeftToRight(Finder finder) {
        super(finder);
    }
    
    //int tRow = row, tCol = col + c;
    protected int getPuzzleRow(int c) {
        return row;
    }

    protected int getPuzzleCol(int c) {
        return col + c;
    }

    protected void initialize(char[][]puzzle, int puzzleWidth, int puzzleHeight, int wordLength) {
        this.puzzle = puzzle;
        this.foundIntable = false;
        this.startRow = 0;
        this.startCol = 0;
        this.lastRow = puzzleHeight;
        this.lastCol = getUpperBoundary(puzzleWidth, wordLength, Direction.FORWARD);
        this.rowIncrementer = 1;
        this.colIncrementer = 1;
        this.sRow = this.sCol = this.eRow = this.eCol = -1;
        //if (Main.EnableDebug && log.isDebugEnabled()) log.debug(String.format("startRow %d => lastRow %d  startCol %d => lastCol %d", startRow, lastRow - 1, startCol, lastCol - 1));
    }

    protected boolean lettersMatch(char wordLetter, int tRow, int tCol) {
        if (Main.EnableTrace && log.isTraceEnabled()) log.trace(String.format("Row %d  Col %d  %c  %c", tRow, tCol, puzzle[tRow][tCol], wordLetter));
        if (wordLetter != puzzle[tRow][tCol]) return false;

        if (this.sRow < 0) { // start tracking position of word
            this.sRow = eRow = tRow;
            this.sCol = tCol;
        }
        this.eCol = tCol; // save last position that was successful

        return true;
    }
}
