package com.dsmoot.finder;

import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.function.Supplier;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.dsmoot.Main;
import com.dsmoot.searcher.DiagonalBottomLeftToTopRight;
import com.dsmoot.searcher.DiagonalBottomRightToTopLeft;
import com.dsmoot.searcher.DiagonalTopLeftToBottomRight;
import com.dsmoot.searcher.DiagonalTopRightToBottomLeft;
import com.dsmoot.searcher.HorizontalLeftToRight;
import com.dsmoot.searcher.HorizontalRightToLeft;
import com.dsmoot.searcher.VerticalBottomToTop;
import com.dsmoot.searcher.VerticalTopToBottom;
import com.dsmoot.solver.OldPuzzleSolver;
import com.dsmoot.util.DurationUtil;

@SuppressWarnings("unused")
public class WordFinder implements Finder {
    private static final Logger log = LoggerFactory.getLogger(WordFinder.class);

    private boolean wordFound = false;
    HorizontalLeftToRight horizontalLeftToRight;
    HorizontalRightToLeft horizontalRightToLeft;
    VerticalTopToBottom verticalTopToBottom;
    VerticalBottomToTop verticalBottomToTop;
    DiagonalTopLeftToBottomRight diagonalTopLeftToBottomRight;
    DiagonalTopRightToBottomLeft diagonalTopRightToBottomLeft;
    DiagonalBottomLeftToTopRight diagonalBottomLeftToTopRight;
    DiagonalBottomRightToTopLeft diagonalBottomRightToTopLeft;
    
    public WordFinder() {
        horizontalLeftToRight = new HorizontalLeftToRight(this);
        horizontalRightToLeft = new HorizontalRightToLeft(this);
        verticalTopToBottom = new VerticalTopToBottom(this);
        verticalBottomToTop = new VerticalBottomToTop(this);
        diagonalTopLeftToBottomRight = new DiagonalTopLeftToBottomRight(this);
        diagonalTopRightToBottomLeft = new DiagonalTopRightToBottomLeft(this);
        diagonalBottomLeftToTopRight = new DiagonalBottomLeftToTopRight(this);
        diagonalBottomRightToTopLeft = new DiagonalBottomRightToTopLeft(this);
    }
    
    public synchronized boolean isWordFound() {
        return wordFound;
    }

    public synchronized void setWordFound(boolean wordFound) {
        this.wordFound = wordFound;
    }

    public String findWord(char[][]puzzle, int puzzleWidth, int puzzleHeight, char[] wordToFind, int wordLength) {
        Instant begin = null, end = null;
        if (Main.EnableInfo && log.isInfoEnabled()) {
            begin = Instant.now();
            log.info("Word search started sync");
        }
        String found = "";
        this.setWordFound(false);

        if (found.isEmpty()) found = this.horizontalLeftToRight.search(puzzle, puzzleWidth, puzzleHeight, wordToFind, wordLength);
        if (found.isEmpty()) found = this.horizontalRightToLeft.search(puzzle, puzzleWidth, puzzleHeight, wordToFind, wordLength);
        if (found.isEmpty()) found = this.verticalTopToBottom.search(puzzle, puzzleWidth, puzzleHeight, wordToFind, wordLength);
        if (found.isEmpty()) found = this.verticalBottomToTop.search(puzzle, puzzleWidth, puzzleHeight, wordToFind, wordLength);
        if (found.isEmpty()) found = this.diagonalTopLeftToBottomRight.search(puzzle, puzzleWidth, puzzleHeight, wordToFind, wordLength);
        if (found.isEmpty()) found = this.diagonalBottomRightToTopLeft.search(puzzle, puzzleWidth, puzzleHeight, wordToFind, wordLength);
        if (found.isEmpty()) found = this.diagonalTopRightToBottomLeft.search(puzzle, puzzleWidth, puzzleHeight, wordToFind, wordLength);
        if (found.isEmpty()) found = this.diagonalBottomLeftToTopRight.search(puzzle, puzzleWidth, puzzleHeight, wordToFind, wordLength);

        if (Main.EnableInfo && log.isInfoEnabled()) {
            end = Instant.now();
            Duration dur = Duration.between(begin, end);
            log.info(String.format("%s %s - sync %s", String.valueOf(wordToFind), found, DurationUtil.toSeconds(dur).toString()));
        }
        return String.format("%s %s", String.valueOf(wordToFind), found);
    }
    
    public CompletableFuture<String> findWordAsync(char[][]puzzle, int puzzleWidth, int puzzleHeight, char[] wordToFind, int wordLength) {
        return CompletableFuture.supplyAsync(new Supplier<String>() {
            @Override
            public String get() {
                Instant begin = null, end = null;
                String found = "";
                setWordFound(false);
                List<CompletableFuture<String>> threads = new ArrayList<CompletableFuture<String>>();
                
                if (Main.EnableInfo && log.isInfoEnabled()) {
                    begin = Instant.now();
                    log.info("Word search started async - Creating threads");
                }
                threads.add(horizontalLeftToRight.searchAsync(puzzle, puzzleWidth, puzzleHeight, wordToFind, wordLength));
                threads.add(horizontalRightToLeft.searchAsync(puzzle, puzzleWidth, puzzleHeight, wordToFind, wordLength));
                threads.add(verticalTopToBottom.searchAsync(puzzle, puzzleWidth, puzzleHeight, wordToFind, wordLength));
                threads.add(verticalBottomToTop.searchAsync(puzzle, puzzleWidth, puzzleHeight, wordToFind, wordLength));
                threads.add(diagonalTopLeftToBottomRight.searchAsync(puzzle, puzzleWidth, puzzleHeight, wordToFind, wordLength));     
                threads.add(diagonalBottomRightToTopLeft.searchAsync(puzzle, puzzleWidth, puzzleHeight, wordToFind, wordLength));     
                threads.add(diagonalTopRightToBottomLeft.searchAsync(puzzle, puzzleWidth, puzzleHeight, wordToFind, wordLength));     
                threads.add(diagonalBottomLeftToTopRight.searchAsync(puzzle, puzzleWidth, puzzleHeight, wordToFind, wordLength));     
                
                // wait for all threads to complete
                try {
                    if (Main.EnableDebug && log.isDebugEnabled()) log.debug("Waiting for threads to complete");
                    CompletableFuture.allOf(threads.toArray(new CompletableFuture[threads.size()])).join();
                    if (Main.EnableDebug && log.isDebugEnabled()) log.debug("Threads completed");
                    
                    for(CompletableFuture<String> thread : threads) {
                        found = thread.get();
                        if (!found.isEmpty()) break;
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                }

                if (Main.EnableInfo && log.isInfoEnabled()) {
                    end = Instant.now();
                    Duration dur = Duration.between(begin, end);
                    log.info(String.format("%s %s - async %s", String.valueOf(wordToFind), found, DurationUtil.toSeconds(dur).toString()));
                }
                return String.format("%s %s", String.valueOf(wordToFind), found);
            }
        });
    }

    public String findWordHorizontalLeftToRight(char[][]puzzle, int puzzleWidth, int puzzleHeight, char[] wordToFind, int wordLength) {
        return this.horizontalLeftToRight.search(puzzle, puzzleWidth, puzzleHeight, wordToFind, wordLength);
    }
    
    public String findWordHorizontalRightToLeft(char[][]puzzle, int puzzleWidth, int puzzleHeight, char[] wordToFind, int wordLength) {
        return this.horizontalRightToLeft.search(puzzle, puzzleWidth, puzzleHeight, wordToFind, wordLength);
    }
    
    public String findWordVerticalTopToBottom(char[][]puzzle, int puzzleWidth, int puzzleHeight, char[] wordToFind, int wordLength) {
        return this.verticalTopToBottom.search(puzzle, puzzleWidth, puzzleHeight, wordToFind, wordLength);
    }
    
    public String findWordVerticalBottomToTop(char[][]puzzle, int puzzleWidth, int puzzleHeight, char[] wordToFind, int wordLength) {
        return this.verticalBottomToTop.search(puzzle, puzzleWidth, puzzleHeight, wordToFind, wordLength);
    }
    
    public String findWordDiagonalTopLeftToBottomRight(char[][]puzzle, int puzzleWidth, int puzzleHeight, char[] wordToFind, int wordLength) {
        return this.diagonalTopLeftToBottomRight.search(puzzle, puzzleWidth, puzzleHeight, wordToFind, wordLength);
    }

    public String findWordDiagonalBottomRightToTopLeft(char[][]puzzle, int puzzleWidth, int puzzleHeight, char[] wordToFind, int wordLength) {
        return this.diagonalBottomRightToTopLeft.search(puzzle, puzzleWidth, puzzleHeight, wordToFind, wordLength);
    }
    
    public String findWordDiagonalBottomLeftToTopRight(char[][]puzzle, int puzzleWidth, int puzzleHeight, char[] wordToFind, int wordLength) {
        return this.diagonalBottomLeftToTopRight.search(puzzle, puzzleWidth, puzzleHeight, wordToFind, wordLength);
    }
    
    public String findWordDiagonalTopRightToBottomLeft(char[][]puzzle, int puzzleWidth, int puzzleHeight, char[] wordToFind, int wordLength) {
        return this.diagonalTopRightToBottomLeft.search(puzzle, puzzleWidth, puzzleHeight, wordToFind, wordLength);
    }
}
