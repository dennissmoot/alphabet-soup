package com.dsmoot.finder;

import java.util.concurrent.CompletableFuture;

public interface Finder {
    public boolean isWordFound();

    public void setWordFound(boolean wordFound);
    
    public String findWord(char[][]puzzle, int puzzleWidth, int puzzleHeight, char[] wordToFind, int wordLength);
    public CompletableFuture<String> findWordAsync(char[][]puzzle, int puzzleWidth, int puzzleHeight, char[] wordToFind, int wordLength);

    public String findWordHorizontalLeftToRight(char[][]puzzle, int puzzleWidth, int puzzleHeight, char[] wordToFind, int wordLength);
    public String findWordHorizontalRightToLeft(char[][]puzzle, int puzzleWidth, int puzzleHeight, char[] wordToFind, int wordLength);
    public String findWordVerticalTopToBottom(char[][]puzzle, int puzzleWidth, int puzzleHeight, char[] wordToFind, int wordLength);
    public String findWordVerticalBottomToTop(char[][]puzzle, int puzzleWidth, int puzzleHeight, char[] wordToFind, int wordLength);
    public String findWordDiagonalTopLeftToBottomRight(char[][]puzzle, int puzzleWidth, int puzzleHeight, char[] wordToFind, int wordLength);
    public String findWordDiagonalBottomRightToTopLeft(char[][]puzzle, int puzzleWidth, int puzzleHeight, char[] wordToFind, int wordLength);
    public String findWordDiagonalBottomLeftToTopRight(char[][]puzzle, int puzzleWidth, int puzzleHeight, char[] wordToFind, int wordLength);
    public String findWordDiagonalTopRightToBottomLeft(char[][]puzzle, int puzzleWidth, int puzzleHeight, char[] wordToFind, int wordLength);
}
