package com.dsmoot.solver;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.charset.Charset;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.dsmoot.Main;
import com.dsmoot.exception.AlphabetSoupException;
import com.dsmoot.util.DurationUtil;

@SuppressWarnings("unused")
public class OldPuzzleSolver implements PuzzleSolver {
	private static final Logger log = LoggerFactory.getLogger(OldPuzzleSolver.class);

	private int tableWidth = 0;
	private int tableHeight = 0;
	private char[][] table;
	private List<char[]> wordsToFind = null;
	private boolean wordFound = false;
	
	public enum Direction { FORWARD, BACKWARD }
	public enum Mode { 
		HORIZONTAL_LEFT_RIGHT,
		HORIZONTAL_RIGHT_LEFT,
		VERTICAL_TOP_BOTTOM,
		VERTICAL_BOTTOM_TOP,
		DIAGONAL_TOP_LEFT_BOTTOM_RIGHT,
		DIAGONAL_BOTTOM_RIGHT_TOP_LEFT,
		DIAGONAL_BOTTOM_LEFT_TOP_RIGHT,
		DIAGONAL_TOP_RIGHT_LEFT_BOTTOM
	}
	
	public OldPuzzleSolver() {
		
	}
	
	public OldPuzzleSolver(String fileName) throws IOException, AlphabetSoupException {
		this.readFile(fileName);
	}
	
	private synchronized boolean isWordFound() {
		return wordFound;
	}

	private synchronized void setWordFound(boolean wordFound) {
		this.wordFound = wordFound;
	}

	private int getUpperBoundary(int table, int word, Direction direction) {
		if (direction == Direction.FORWARD) return table - word + 1;
		return word - 2;
	}
	
	public String findHorizontalLeftToRight(char[] wordToFind) {
		if (Main.EnableDebug && log.isDebugEnabled()) log.debug(String.format("%nfindHorizontalLeftToRight %s", String.valueOf(wordToFind)));
		int wordLength = wordToFind.length;
		int startRow = 0;
		int startCol = 0;
		// the lastRow and lastCol will contain values that outside the boundary for searching for words
		int lastRow = tableHeight;
		int lastCol = getUpperBoundary(tableWidth, wordLength, Direction.FORWARD);
		int rowIncrementer = 1;
		int colIncrementer = 1;
		if (Main.EnableDebug && log.isDebugEnabled()) log.debug(String.format("startRow %d => lastRow %d  startCol %d => lastCol %d", startRow, lastRow - 1, startCol, lastCol - 1));
		
		int sRow = -1, sCol = -1, eRow = -1, eCol = -1;
		boolean foundIntable = false;
		char w, t;
		int row = startRow;
		while (row != lastRow) {
			int col = startCol;
			while (col != lastCol) {
				// check to see if word was found in another thread
				// placed where for now to improve performance reasons
				if (this.isWordFound()) {
					if (Main.EnableDebug && log.isDebugEnabled()) log.debug("findHorizontalLeftToRight - Word found in another thread");
					return "";
				}

				// search for word
				boolean found = true;
				// intialize tracking values
				sRow = sCol = eRow = eCol = -1;
				for (int c = 0; c < wordLength; c++) {
					int tRow = row, tCol = col + c;
					w = wordToFind[c];
					t = table[tRow][tCol];
					
                    if (Main.EnableTrace && log.isTraceEnabled()) log.trace(String.format("Row %d  Col %d  %c  %c", tRow, tCol, t, w));
					if (w != t) {
						found = false; // set flag that word not found
						break;
					}
					else if (sRow < 0) { // start tracking position of word
						sRow = eRow = tRow;
						sCol = tCol;
					}
					eCol = tCol; // save last position that was successful
				}
				// if word found then set flags to break out of loops
				if (found) {
					foundIntable = true;
					break;
				}
				col += colIncrementer;
			}

			if (foundIntable) break;
			row += rowIncrementer;
		}
		
		if (!foundIntable) return "";
		
		this.setWordFound(true);
		return String.format("%d:%d %d:%d", sRow, sCol, eRow, eCol);
	}

	public CompletableFuture<String> findHorizontalLeftToRightAsync(char[] wordToFind) {
		return CompletableFuture.completedFuture(findHorizontalLeftToRight(wordToFind));
	}
	
	private String findHorizontalRightToLeft(char[] wordToFind) {
		if (Main.EnableDebug && log.isDebugEnabled()) log.debug(String.format("%nfindHorizontalToRightToLeft %s", String.valueOf(wordToFind)));
		int wordLength = wordToFind.length;
        int startRow = 0;
        int startCol = tableWidth - 1;
		// the lastRow and lastCol will contain values that outside the boundary for searching for words
		int lastRow = tableHeight;
		int lastCol = getUpperBoundary(tableWidth, wordLength, Direction.BACKWARD);
        int rowIncrementer = 1;
        int colIncrementer = -1;
        if (Main.EnableDebug && log.isDebugEnabled()) log.debug(String.format("startRow %d => lastRow %d  startCol %d => lastCol %d", startRow, lastRow - 1, startCol, lastCol + 1));

		int sRow = -1, sCol = -1, eRow = -1, eCol = -1;
		boolean foundIntable = false;
		char w, t;
		int row = startRow;
		while(row != lastRow) {
			int col = startCol;
			while(col != lastCol) {
				// check to see if word was found in another thread
				if (this.isWordFound()) {
					if (Main.EnableDebug && log.isDebugEnabled()) log.debug("findHorizontalRightToLeft - Word found in another thread");
					return "";
				}

				// search for word
				boolean found = true; // assume word is found
				sRow = sCol = eRow = eCol = -1;
				for (int c = 0; c < wordLength; c++) {
					int tRow = row, tCol = col - c;
					w = wordToFind[c];
					t = table[tRow][tCol];

                    if (Main.EnableTrace && log.isTraceEnabled()) log.trace(String.format("Row %d  Col %d  %c  %c", tRow, tCol, t, w));
					if (w != t) {
						found = false; // set flag that word not found
						break;
					}
					else if (sRow < 0) { // start tracking position of word
						sRow = eRow = tRow;
						sCol = tCol;
					}
					eCol = tCol; // save last position that was successful
				}
				// if word found then set flags to break out of loops
				if (found) {
					foundIntable = true;
					break;
				}
				col += colIncrementer;
			}

			if (foundIntable) break;
			row += rowIncrementer;
		}
		
		if (!foundIntable) return "";
		
		this.setWordFound(true);
		return String.format("%d:%d %d:%d", sRow, sCol, eRow, eCol);
	}

	private CompletableFuture<String> findHorizontalRightToLeftAsync(char[] wordToFind) {
		return CompletableFuture.completedFuture(this.findHorizontalRightToLeft(wordToFind));
	}
	
	public String findVerticalTopToBottom(char[] wordToFind) {
		if (Main.EnableDebug && log.isDebugEnabled()) log.debug(String.format("%nfindVerticalTopToBottom %s", String.valueOf(wordToFind)));
		int wordLength = wordToFind.length;
        int startRow = 0;
        int startCol = 0;
		// the lastRow and lastCol will contain values that outside the boundary for searching for words
		int lastRow = getUpperBoundary(tableHeight, wordLength, Direction.FORWARD);
		int lastCol = tableWidth;
        
        int rowIncrementer = 1;
        int colIncrementer = 1;
        if (Main.EnableDebug && log.isDebugEnabled()) log.debug(String.format("startRow %d => lastRow %d  startCol %d => lastCol %d", startRow, lastRow - 1, startCol, lastCol - 1));

		int sRow = -1, sCol = -1, eRow = -1, eCol = -1;
		boolean foundIntable = false;
		char w, t;
		int row = startRow;
		while (row != lastRow) {
			int col = startCol;
			while (col != lastCol) {
				// search for word
				boolean found = true; // assume word is found
				sRow = sCol = eRow = eCol = -1;
				for (int c = 0; c < wordLength; c++) {
					// check to see if word was found in another thread
					if (this.isWordFound()) {
						if (Main.EnableDebug && log.isDebugEnabled()) log.debug("findVerticalTopToBottom - Word found in another thread");
						return "";
					}

					int tRow = row + c, tCol = col;
					w = wordToFind[c];
					t = table[tRow][tCol];
					
                    if (Main.EnableTrace && log.isTraceEnabled()) log.trace(String.format("Row %d  Col %d  %c  %c", tRow, tCol, t, w));
					if (w != t) {
						found = false; // set flag that word not found
						break;
					}
					else if (sRow < 0) { // start tracking position of word
						sRow = tRow;
						sCol = eCol = tCol;
					}
					eRow = tRow; // save last position that was successful
				}
				// if word found then set flags to break out of loops
				if (found) {
					foundIntable = true;
					break;
				}
				col += colIncrementer;
			}

			if (foundIntable) break;
			row += rowIncrementer;
		}
		
		if (!foundIntable) return "";
		
		this.setWordFound(true);
		return String.format("%d:%d %d:%d", sRow, sCol, eRow, eCol);
	}

	public CompletableFuture<String> findVerticalTopToBottomAsync(char[] wordToFind) {
		return CompletableFuture.completedFuture(this.findVerticalTopToBottom(wordToFind));
	}
	
	public String findVerticalBottomToTop(char[] wordToFind) {
		if (Main.EnableDebug && log.isDebugEnabled()) log.debug(String.format("%nfindVerticalBottomTop %s", String.valueOf(wordToFind)));
		int wordLength = wordToFind.length;
        int startRow =  tableHeight - 1;
        int startCol = 0;
		// the lastRow and lastCol will contain values that outside the boundary for searching for words
		int lastRow = getUpperBoundary(tableHeight, wordLength, Direction.BACKWARD);
		int lastCol = tableWidth;
        
        int rowIncrementer = -1;
        int colIncrementer = 1;
        if (Main.EnableDebug && log.isDebugEnabled()) log.debug(String.format("startRow %d => lastRow %d  startCol %d => lastCol %d", startRow, lastRow - 1, startCol, lastCol - 1));

		int sRow = -1, sCol = -1, eRow = -1, eCol = -1;
		boolean foundIntable = false;
		char w, t;
		int row = startRow;
		while (row != lastRow) {
			int col = startCol;
			while (col != lastCol) {
				// search for word
				boolean found = true; // assume word is found
				sRow = sCol = eRow = eCol = -1;
				for (int c = 0; c < wordLength; c++) {
					// check to see if word was found in another thread
					if (this.isWordFound()) {
						if (Main.EnableDebug && log.isDebugEnabled()) log.debug("findVerticalBottomToTop - Word found in another thread");
						return "";
					}

					int tRow = row - c, tCol = col;
					w = wordToFind[c];
					t = table[tRow][tCol];
					
                    if (Main.EnableTrace && log.isTraceEnabled()) log.trace(String.format("Row %d  Col %d  %c  %c", tRow, tCol, t, w));
					if (w != t) {
						found = false; // set flag that word not found
						break;
					}
					else if (sRow < 0) { // start tracking position of word
						sRow = tRow;
						sCol = eCol = tCol;
					}
					eRow = tRow; // save last position that was successful
				}
				// if word found then set flags to break out of loops
				if (found) {
					foundIntable = true;
					break;
				}
				col += colIncrementer;
			}

			if (foundIntable) break;
			row += rowIncrementer;
		}
		
		if (!foundIntable) return "";
		
		this.setWordFound(true);
		return String.format("%d:%d %d:%d", sRow, sCol, eRow, eCol);
	}

	public CompletableFuture<String> findVerticalBottomToTopAsync(char[] wordToFind) {
		return CompletableFuture.completedFuture(this.findVerticalBottomToTop(wordToFind));
	}

	public String findDiagonalTopLeftToBottomRight(char[] wordToFind) {
		if (Main.EnableDebug && log.isDebugEnabled()) log.debug(String.format("%nfindDiagonalTopLeftToBottomRight %s", String.valueOf(wordToFind)));
		int wordLength = wordToFind.length;
        int startRow = 0;
        int startCol = 0;
        int lastRow = getUpperBoundary(tableHeight, wordLength, Direction.FORWARD);
        int lastCol = getUpperBoundary(tableWidth, wordLength, Direction.FORWARD);
        int rowIncrementer = 1;
        int colIncrementer = 1;
        if (Main.EnableDebug && log.isDebugEnabled()) log.debug(String.format("startRow %d => lastRow %d  startCol %d => lastCol %d", startRow, lastRow - 1, startCol, lastCol - 1));

		int sRow = -1, sCol = -1, eRow = -1, eCol = -1;
		boolean foundIntable = false;
		char w, t;
		int row = startRow;
		while (row != lastRow) {
			int col = startCol;
			while (col != lastCol) {
				// search for word
				boolean found = true; // assume word is found
				sRow = sCol = eRow = eCol = -1;
				for (int c = 0; c < wordLength; c++) {
					// check to see if word was found in another thread
					if (this.isWordFound()) {
						if (Main.EnableDebug && log.isDebugEnabled()) log.debug("findDiagonalTopLeftToBottomRight - Word found in another thread");
						return "";
					}

					int tRow = row + c, tCol = col + c;
					w = wordToFind[c];
					t = table[tRow][tCol];

                    if (Main.EnableTrace && log.isTraceEnabled()) log.trace(String.format("Row %d  Col %d  %c  %c", tRow, tCol, t, w));
					if (w != t) {
						found = false; // set flag that word not found
						break;
					}
					else if (sRow < 0) { // start tracking position of word
						sRow = tRow;
						sCol = tCol;
					}
					eRow = tRow;
					eCol = tCol; // save last position that was successful
				}
				// if word found then set flags to break out of loops
				if (found) {
					foundIntable = true;
					break;
				}
				col += colIncrementer;
			}

			if (foundIntable) break;
			row += rowIncrementer;
		}
		
		if (!foundIntable) return "";
		
		this.setWordFound(true);
		return String.format("%d:%d %d:%d", sRow, sCol, eRow, eCol);
	}

	public CompletableFuture<String> findDiagonalTopLeftToBottomRightAsync(char[] wordToFind) {
		return CompletableFuture.completedFuture(this.findDiagonalTopLeftToBottomRight(wordToFind));
	}
	
	public String findDiagonalBottomRightToTopLeft(char[] wordToFind) {
		if (Main.EnableDebug && log.isDebugEnabled()) log.debug(String.format("%nfindDiagonalBottomRightToTopLeft %s", String.valueOf(wordToFind)));
		int wordLength = wordToFind.length;
        int startRow = tableHeight - 1;
        int startCol = tableWidth - 1;
        int lastRow = getUpperBoundary(tableHeight, wordLength, Direction.BACKWARD);
        int lastCol = getUpperBoundary(tableWidth, wordLength, Direction.BACKWARD);
        int rowIncrementer = -1;
        int colIncrementer = -1;
        if (Main.EnableDebug && log.isDebugEnabled()) log.debug(String.format("startRow %d => lastRow %d  startCol %d => lastCol %d", startRow, lastRow - 1, startCol, lastCol - 1));
		
		int sRow = -1, sCol = -1, eRow = -1, eCol = -1;
		boolean foundIntable = false;
		char w, t;
		int row = startRow;
		while (row != lastRow) {
			int col = startCol;
			while (col != lastCol) {
				// check to see if word was found in another thread
				if (this.isWordFound()) {
					if (Main.EnableDebug && log.isDebugEnabled()) log.debug("findDiagonalBottomRightToTopLeft - Word found in another thread");
					return "";
				}

				// search for word
				boolean found = true; // assume word is found
				sRow = sCol = eRow = eCol = -1;
				for (int c = 0; c < wordLength; c++) {
					int tRow = row - c, tCol = col - c;
					w = wordToFind[c];
					t = table[tRow][tCol];
					
                    if (Main.EnableTrace && log.isTraceEnabled()) log.trace(String.format("Row %d  Col %d  %c  %c", tRow, tCol, t, w));
					if (w != t) {
						found = false; // set flag that word not found
						break;
					}
					else if (sRow < 0) { // start tracking position of word
						sRow = tRow;
						sCol = tCol;
					}
					eRow = tRow;
					eCol = tCol; // save last position that was successful
				}
				// if word found then set flags to break out of loops
				if (found) {
					foundIntable = true;
					break;
				}
				col += colIncrementer;
			}

			if (foundIntable) break;
			row += rowIncrementer;
		}
		
		if (!foundIntable) return "";
		
		this.setWordFound(true);
		return String.format("%d:%d %d:%d", sRow, sCol, eRow, eCol);
	}

	public CompletableFuture<String> findDiagonalBottomRightToTopLeftAsync(char[] wordToFind) {
		return CompletableFuture.completedFuture(findDiagonalBottomRightToTopLeft(wordToFind));
	}
	
	public String findDiagonalBottomLeftToTopRight(char[] wordToFind) {
		if (Main.EnableDebug && log.isDebugEnabled()) log.debug(String.format("%nfindDiagonalBottomLeftToTopRight %s", String.valueOf(wordToFind)));
		int wordLength = wordToFind.length;
		int startRow = tableHeight - 1;
		int startCol = 0;
        int lastRow = getUpperBoundary(tableHeight, wordLength, Direction.BACKWARD);
        int lastCol = getUpperBoundary(tableWidth, wordLength, Direction.FORWARD);
		int rowIncrementer = -1;
        int colIncrementer = 1;
        if (Main.EnableDebug && log.isDebugEnabled()) log.debug(String.format("startRow %d => lastRow %d  startCol %d => lastCol %d", startRow, lastRow - 1, startCol, lastCol - 1));
		
		int sRow = -1, sCol = -1, eRow = -1, eCol = -1;
		boolean foundIntable = false;
		char w, t;
		int row = startRow;
		while (row != lastRow) {
			int col = startCol;
			while (col != lastCol) {
				// check to see if word was found in another thread
				if (this.isWordFound()) {
					if (Main.EnableDebug && log.isDebugEnabled()) log.debug("findDiagonalBottomLeftToTopRight - Word found in another thread");
					return "";
				}

				// search for word
				boolean found = true; // assume word is found
				sRow = sCol = eRow = eCol = -1;
				for (int c = 0; c < wordLength; c++) {
					int tRow = row - c, tCol = col + c;
					w = wordToFind[c];
					t = table[tRow][tCol];

                    if (Main.EnableTrace && log.isTraceEnabled()) log.trace(String.format("Row %d  Col %d  %c  %c", tRow, tCol, t, w));
					if (w != t) {
						found = false; // set flag that word not found
						break;
					}
					else if (sRow < 0) { // start tracking position of word
						sRow = tRow;
						sCol = tCol;
					}
					eRow = tRow;
					eCol = tCol; // save last position that was successful
				}
				// if word found then set flags to break out of loops
				if (found) {
					foundIntable = true;
					break;
				}
				col += colIncrementer;
			}

			if (foundIntable) break;
			row += rowIncrementer;
		}
		
		if (!foundIntable) return "";
		
		this.setWordFound(true);
		return String.format("%d:%d %d:%d", sRow, sCol, eRow, eCol);
	}

	public CompletableFuture<String> findDiagonalBottomLeftToTopRightAsync(char[] wordToFind) {
		return CompletableFuture.completedFuture(findDiagonalBottomLeftToTopRight(wordToFind));
	}
	
	public String findDiagonalTopRightToBottomLeft(char[] wordToFind) {
		if (Main.EnableDebug && log.isDebugEnabled()) log.debug(String.format("%nfindDiagonalTopRightToBottomLeft %s", String.valueOf(wordToFind)));
		int wordLength = wordToFind.length;
        int startRow = 0;
        int startCol = tableWidth - 1;
        int lastRow = getUpperBoundary(tableHeight, wordLength, Direction.FORWARD);
        int lastCol = getUpperBoundary(tableWidth, wordLength, Direction.BACKWARD);
        int rowIncrementer = 1;
        int colIncrementer = -1;
        if (Main.EnableDebug && log.isDebugEnabled()) log.debug(String.format("startRow %d => lastRow %d  startCol %d => lastCol %d", startRow, lastRow - 1, startCol, lastCol - 1));

		int sRow = -1, sCol = -1, eRow = -1, eCol = -1;
		boolean foundIntable = false;
		char w, t;
		int row = startRow;
		while (row != lastRow) {
			int col = startCol;
			while (col != lastCol) {
				// check to see if word was found in another thread
				if (this.isWordFound()) {
					if (Main.EnableDebug && log.isDebugEnabled()) log.debug("findDiagonalTopRightToBottomLeft - Word found in another thread");
					return "";
				}

				// search for word
				boolean found = true; // assume word is found
				sRow = sCol = eRow = eCol = -1;
				for (int c = 0; c < wordLength; c++) {
					int tRow = row + c, tCol = col - c;
					w = wordToFind[c];
					t = table[tRow][tCol];

                    if (Main.EnableTrace && log.isTraceEnabled()) log.trace(String.format("Row %d  Col %d  %c  %c", tRow, tCol, t, w));
					if (w != t) {
						found = false; // set flag that word not found
						break;
					}
					else if (sRow < 0) { // start tracking position of word
						sRow = tRow;
						sCol = tCol;
					}
					eRow = tRow;
					eCol = tCol; // save last position that was successful
				}
				// if word found then set flags to break out of loops
				if (found) {
					foundIntable = true;
					break;
				}
				col += colIncrementer;
			}

			if (foundIntable) break;
			row += rowIncrementer;
		}
		
		if (!foundIntable) return "";
		
		this.setWordFound(true);
		return String.format("%d:%d %d:%d", sRow, sCol, eRow, eCol);
	}

	public CompletableFuture<String> findDiagonalTopRightToBottomLeftAsync(char[] wordToFind) {
		return CompletableFuture.completedFuture(findDiagonalTopRightToBottomLeft(wordToFind));
	}
	

	public String findWordAsync(char[] wordToFind) {
		String found = "";
		this.setWordFound(false);
		List<CompletableFuture<String>> threads = new ArrayList<CompletableFuture<String>>();
		
		threads.add(findHorizontalLeftToRightAsync(wordToFind));
		threads.add(findHorizontalRightToLeftAsync(wordToFind));
		threads.add(findVerticalTopToBottomAsync(wordToFind));
		threads.add(findVerticalBottomToTopAsync(wordToFind));
		threads.add(findDiagonalTopLeftToBottomRightAsync(wordToFind));		
		threads.add(findDiagonalBottomRightToTopLeftAsync(wordToFind));		
		threads.add(findDiagonalTopRightToBottomLeftAsync(wordToFind));		
		threads.add(findDiagonalBottomLeftToTopRightAsync(wordToFind));		
		
		// wait for all threads to complete
		try {
            if (Main.EnableDebug && log.isDebugEnabled()) log.debug("Waiting for threads to complete");
			CompletableFuture.allOf(threads.toArray(new CompletableFuture[threads.size()])).join();
            if (Main.EnableDebug && log.isDebugEnabled()) log.debug("Threads completed");

            for(CompletableFuture<String> thread : threads) {
				found = thread.get();
				if (!found.isEmpty()) break;
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {
			e.printStackTrace();
		}

		return String.format("%s %s", String.valueOf(wordToFind), found);
	}
	
	public String findWord(char[] wordToFind) {
		String found = "";
		this.setWordFound(false);

		if (found.isEmpty()) found = findHorizontalLeftToRight(wordToFind);
		if (found.isEmpty()) found = findHorizontalRightToLeft(wordToFind);
		if (found.isEmpty()) found = findVerticalTopToBottom(wordToFind);
		if (found.isEmpty()) found = findVerticalBottomToTop(wordToFind);
		if (found.isEmpty()) found = findDiagonalTopLeftToBottomRight(wordToFind);		
		if (found.isEmpty()) found = findDiagonalBottomRightToTopLeft(wordToFind);		
		if (found.isEmpty()) found = findDiagonalTopRightToBottomLeft(wordToFind);		
		if (found.isEmpty()) found = findDiagonalBottomLeftToTopRight(wordToFind);		

		return String.format("%s %s", String.valueOf(wordToFind), found);
	}
	
	public String findWordHorizontalLeftToRight(char[] wordToFind) {
		this.setWordFound(false);
		return this.findHorizontalLeftToRight(wordToFind);
	}
	
	public String findWordHorizontalRightToLeft(char[] wordToFind) {
		this.setWordFound(false);
		return this.findHorizontalRightToLeft(wordToFind);
	}
	
	public String findWordVerticalTopToBottom(char[] wordToFind) {
		this.setWordFound(false);
		return this.findVerticalTopToBottom(wordToFind);
	}
	
	public String findWordVerticalBottomToTop(char[] wordToFind) {
		this.setWordFound(false);
		return this.findVerticalBottomToTop(wordToFind);
	}
	
	public String findWordDiagonalTopLeftToBottomRight(char[] wordToFind) {
		this.setWordFound(false);
		return this.findDiagonalTopLeftToBottomRight(wordToFind);
	}

	public String findWordDiagonalBottomRightToTopLeft(char[] wordToFind) {
		this.setWordFound(false);
		return this.findDiagonalBottomRightToTopLeft(wordToFind);
	}
	
	public String findWordDiagonalBottomLeftToTopRight(char[] wordToFind) {
		this.setWordFound(false);
		return findDiagonalBottomLeftToTopRight(wordToFind);
	}
	
	public String findWordDiagonalTopRightToBottomLeft(char[] wordToFind) {
		this.setWordFound(false);
		return findDiagonalTopRightToBottomLeft(wordToFind);
	}

	public void readFile(String fileName) throws IOException, AlphabetSoupException {
		if (Main.EnableDebug && log.isDebugEnabled()) log.debug(fileName);
		File in = new File(fileName);
		if (! in.exists()) {
			throw new FileNotFoundException(fileName + "not found");
		}
		try (BufferedReader reader = new BufferedReader(new FileReader(in, Charset.defaultCharset()))) {
			// get the table dimensions
			String line = reader.readLine();
            if (line == null) return; // end of file detected
			line = line.toLowerCase(Locale.US);
			String[] values = line.split("x");
			tableWidth = Integer.parseInt(values[0]);
			tableHeight = Integer.parseInt(values[1]);
			if (Main.EnableDebug && log.isDebugEnabled()) log.debug("Width " + tableWidth + "   Height " + tableHeight);
			// allocate 2 dim table
			table = new char[tableHeight][tableWidth];
			
			// populate table
			for (int t = 0; t < tableHeight; t++) {
				line = reader.readLine();
				if (line == null) break;
				line = line.replaceAll("\\s", "").toUpperCase(Locale.US);
				int len = line.length();
				if (len != tableWidth) {
					throw new AlphabetSoupException(String.format("Invalid input detected on row %d  len %d", t + 2, len));
				}
				char[] chars = line.toCharArray();
				for (int i = 0; i < len; i++) table[t][i] = chars[i];
			}
            if (line == null) return; // end of file detected

			// read words to find
            wordsToFind = new ArrayList<char[]>();
			while((line = reader.readLine()) != null) {
				if (!line.startsWith("#")) wordsToFind.add(line.replace(" ", "").toUpperCase(Locale.US).toCharArray());
			}
			
			if (Main.EnableDebug && log.isDebugEnabled()) {
				for(int row = 0; row < this.tableHeight; row++) {
					log.debug(String.format("%02d %s", row, String.valueOf(table[row])));
				}
					
			}
		}
		catch(IOException e) {
			throw e;
		}
	}

	public void solvePuzzleAsync(String fileName) {
		try {
            Instant begin = Instant.now();
			readFile(fileName);
			
			for(char[] wordtoFind : wordsToFind)
				System.out.println(findWordAsync(wordtoFind));
			Instant end = Instant.now();
			Duration dur = Duration.between(begin, end);
			if (Main.EnableInfo && log.isInfoEnabled()) log.info("ASync " + String.format("%s", DurationUtil.toSeconds(dur).toString()));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	public void solvePuzzle(String fileName) {
		try {
            Instant begin = Instant.now();
			readFile(fileName);
			
			for(char[] wordtoFind : wordsToFind)
				System.out.println(findWord(wordtoFind));
			Instant end = Instant.now();
			Duration dur = Duration.between(begin, end);
			if (Main.EnableInfo && log.isInfoEnabled()) log.info("ASync " + String.format("%s", DurationUtil.toSeconds(dur).toString()));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
}
