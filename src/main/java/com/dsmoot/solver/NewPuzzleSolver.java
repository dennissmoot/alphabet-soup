package com.dsmoot.solver;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.charset.Charset;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.dsmoot.Main;
import com.dsmoot.exception.AlphabetSoupException;
import com.dsmoot.finder.WordFinder;
import com.dsmoot.util.DurationUtil;

@SuppressWarnings("unused")
public class NewPuzzleSolver implements PuzzleSolver {
    private static final Logger log = LoggerFactory.getLogger(NewPuzzleSolver.class);
    private int puzzleWidth = 0;
    private int puzzleHeight = 0;
    private char[][] puzzle;
    private List<char[]> wordsToFind = null;
    WordFinder wordFinder = new WordFinder();

    public NewPuzzleSolver() {

    }

    public NewPuzzleSolver(String fileName) throws IOException, AlphabetSoupException {
        this.readFile(fileName);
    }

    public void readFile(String fileName) throws IOException, AlphabetSoupException {
        if (Main.EnableDebug && log.isDebugEnabled()) log.debug(fileName);
        File in = new File(fileName);
        if (! in.exists()) {
            throw new FileNotFoundException(fileName + "not found");
        }
        try (BufferedReader reader = new BufferedReader(new FileReader(in, Charset.defaultCharset()))) {
            // get the puzzle dimensions
            String line = reader.readLine();
            if (line == null) return; // end of file detected
            line = line.toLowerCase(Locale.US);
            String[] values = line.split("x");
            puzzleWidth = Integer.parseInt(values[0]);
            puzzleHeight = Integer.parseInt(values[1]);
            if (Main.EnableDebug && log.isDebugEnabled()) log.debug("Width " + puzzleWidth + "   Height " + puzzleHeight);
            // allocate 2 dim puzzle
            puzzle = new char[puzzleHeight][puzzleWidth];

            // populate puzzle
            for (int t = 0; t < puzzleHeight; t++) {
                line = reader.readLine();
                if (line == null) break;
                line = line.replaceAll("\\s", "").toUpperCase(Locale.US);
                int len = line.length();
                if (len != puzzleWidth) {
                    throw new AlphabetSoupException(String.format("Invalid input detected on row %d  len %d", t + 2, len));
                }
                char[] chars = line.toCharArray();
                for (int i = 0; i < len; i++) puzzle[t][i] = chars[i];
            }
            if (line == null) return; // end of file detected

            // read words to find
            wordsToFind = new ArrayList<char[]>();
            while((line = reader.readLine()) != null) {
                if (!line.startsWith("#")) wordsToFind.add(line.replace(" ", "").toUpperCase(Locale.US).toCharArray());
            }

            if (Main.EnableDebug && log.isDebugEnabled()) {
                for(int row = 0; row < this.puzzleHeight; row++) {
                    log.debug(String.format("%02d %s", row, String.valueOf(puzzle[row])));
                }

            }
        }
        catch(IOException e) {
            throw e;
        }
    }

    public void solvePuzzleAsync(String fileName) {
        try {
            readFile(fileName);
            if (Main.EnableInfo && log.isInfoEnabled()) log.info("ASync Finding Words");
            Instant begin = Instant.now();

            // populate list of threads
            Map<Integer, CompletableFuture<String>> threads = new TreeMap<Integer, CompletableFuture<String>>();
            int cnt = 0;
            for(char[] wordtoFind : wordsToFind) {
                threads.put(Integer.valueOf(++cnt), new WordFinder().findWordAsync(puzzle, puzzleWidth, puzzleHeight, wordtoFind, wordtoFind.length));
            }
            if (Main.EnableDebug && log.isDebugEnabled()) log.debug(cnt + " word threads created");

            try {
                // wait for all thread to complete
                if (Main.EnableDebug && log.isDebugEnabled()) log.debug("Waiting for threads to complete");
                CompletableFuture.allOf(threads.values().toArray(new CompletableFuture[threads.size()])).join();
                if (Main.EnableDebug && log.isDebugEnabled()) log.debug("Threads completed");

                // output data from threads
                if (Main.EnableDebug && log.isDebugEnabled()) log.debug("Outputting data for " + threads.entrySet().size() + " threads");
                for(Entry<Integer, CompletableFuture<String>> thread : threads.entrySet()) {
                    System.out.println(thread.getValue().get());
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }

            Instant end = Instant.now();
            Duration dur = Duration.between(begin, end);
            if (Main.EnableInfo && log.isInfoEnabled()) log.info("ASync " + String.format("%s", DurationUtil.toSeconds(dur).toString()));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void solvePuzzle(String fileName) {
        try {
            readFile(fileName);
            if (Main.EnableInfo && log.isInfoEnabled()) log.info("Sync Finding Words");
            Instant begin = Instant.now();

            for(char[] wordtoFind : wordsToFind)
                System.out.println(wordFinder.findWord(puzzle, puzzleWidth, puzzleHeight, wordtoFind, wordtoFind.length));

            Instant end = Instant.now();
            Duration dur = Duration.between(begin, end);
            if (Main.EnableInfo && log.isInfoEnabled()) log.info("Sync " + String.format("%s", DurationUtil.toSeconds(dur).toString()));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String findWordHorizontalLeftToRight(char[] wordToFind) {
        wordFinder.setWordFound(false);
        return wordFinder.findWordHorizontalLeftToRight(puzzle, puzzleWidth, puzzleHeight, wordToFind, wordToFind.length);
    }

    public String findWordHorizontalRightToLeft(char[] wordToFind) {
        wordFinder.setWordFound(false);
        return wordFinder.findWordHorizontalRightToLeft(puzzle, puzzleWidth, puzzleHeight, wordToFind, wordToFind.length);
    }

    public String findWordVerticalTopToBottom(char[] wordToFind) {
        wordFinder.setWordFound(false);
        return wordFinder.findWordVerticalTopToBottom(puzzle, puzzleWidth, puzzleHeight, wordToFind, wordToFind.length);
    }

    public String findWordVerticalBottomToTop(char[] wordToFind) {
        wordFinder.setWordFound(false);
        return wordFinder.findWordVerticalBottomToTop(puzzle, puzzleWidth, puzzleHeight, wordToFind, wordToFind.length);
    }

    public String findWordDiagonalTopLeftToBottomRight(char[] wordToFind) {
        wordFinder.setWordFound(false);
        return wordFinder.findWordDiagonalTopLeftToBottomRight(puzzle, puzzleWidth, puzzleHeight, wordToFind, wordToFind.length);
    }

    public String findWordDiagonalBottomRightToTopLeft(char[] wordToFind) {
        wordFinder.setWordFound(false);
        return wordFinder.findWordDiagonalBottomRightToTopLeft(puzzle, puzzleWidth, puzzleHeight, wordToFind, wordToFind.length);
    }

    public String findWordDiagonalBottomLeftToTopRight(char[] wordToFind) {
        wordFinder.setWordFound(false);
        return wordFinder.findWordDiagonalBottomLeftToTopRight(puzzle, puzzleWidth, puzzleHeight, wordToFind, wordToFind.length);
    }

    public String findWordDiagonalTopRightToBottomLeft(char[] wordToFind) {
        wordFinder.setWordFound(false);
        return wordFinder.findWordDiagonalTopRightToBottomLeft(puzzle, puzzleWidth, puzzleHeight, wordToFind, wordToFind.length);
    }
}
