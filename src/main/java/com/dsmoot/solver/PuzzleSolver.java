package com.dsmoot.solver;

import java.io.IOException;
import com.dsmoot.exception.AlphabetSoupException;

public interface PuzzleSolver {

    public void readFile(String fileName) throws IOException, AlphabetSoupException;
    public void solvePuzzleAsync(String fileName);
    public void solvePuzzle(String fileName);
    public String findWordHorizontalLeftToRight(char[] wordToFind);
    public String findWordHorizontalRightToLeft(char[] wordToFind);
    public String findWordVerticalTopToBottom(char[] wordToFind);
    public String findWordVerticalBottomToTop(char[] wordToFind);
    public String findWordDiagonalTopLeftToBottomRight(char[] wordToFind);
    public String findWordDiagonalBottomRightToTopLeft(char[] wordToFind);
    public String findWordDiagonalBottomLeftToTopRight(char[] wordToFind);
    public String findWordDiagonalTopRightToBottomLeft(char[] wordToFind);
}
