package com.dsmoot.exception;

public class AlphabetSoupException extends RuntimeException {
	private static final long serialVersionUID = 1L;

	public AlphabetSoupException() {
		super();
	}

	public AlphabetSoupException(String message) {
		super(message);
	}
}
