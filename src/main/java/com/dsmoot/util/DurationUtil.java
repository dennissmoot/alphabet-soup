package com.dsmoot.util;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.Duration;

public class DurationUtil {
    public static BigDecimal toSeconds(Duration dur) {
        long l = dur.toNanos();
        BigDecimal d = new BigDecimal(l).setScale(4);
        d = d .divide(new BigDecimal(1000000000), RoundingMode.HALF_UP);
        return d;
    }
}
