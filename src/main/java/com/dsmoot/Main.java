package com.dsmoot;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.dsmoot.solver.NewPuzzleSolver;
import com.dsmoot.solver.OldPuzzleSolver;
import com.dsmoot.solver.PuzzleSolver;

@SuppressWarnings("unused")
public class Main {
    private static final Logger log = LoggerFactory.getLogger(Main.class);
    
    // variables used to compile out debug statements via optimization
    public static final boolean EnableTrace = false;
    public static final boolean EnableDebug = false;
    public static final boolean EnableInfo = false;

	public static void main(String[] args) {
	    boolean useNewPuzzleSolver = true;
	    boolean useAsync = false;
        PuzzleSolver puzzleSolver = null;
	    
        if (args.length > 0) {
			for (int i = 0; i < args.length; i++) {
			    switch(args[i].charAt(0)) {
			    case '/':
			    case '-':
			        if ("old".equalsIgnoreCase(args[i].substring(1))) 
			            useNewPuzzleSolver = false;
			        else if ("async".equalsIgnoreCase(args[i].substring(1)))
			            useAsync = true;
			        break;
			    default:
                    if (puzzleSolver == null) puzzleSolver = useNewPuzzleSolver ? new NewPuzzleSolver() : new OldPuzzleSolver();
                    if (useAsync) {
                        if (Main.EnableInfo && log.isInfoEnabled()) log.info("Executing " + (useNewPuzzleSolver ? "NewPuzzleSolver" : "OldPuzzleSolver") + " in async mode");
                        puzzleSolver.solvePuzzleAsync(args[i]);
                    }
                    else {
                        if (Main.EnableInfo && log.isInfoEnabled()) log.info("Executing " + (useNewPuzzleSolver ? "NewPuzzleSolver" : "OldPuzzleSolver") + " in std mode");
                        puzzleSolver.solvePuzzle(args[i]);
                    }
			        break;
			    }
			}
		}
		else {
			System.err.println("Please specify an input file name.");
            System.err.println("Optional parameters are:");
            System.err.println("/old - Use old PuzzleSolver logic");
            System.err.println("/async - Use asynchronous PuzzleSolver logic");
		}
	}
}
