@REM NAVIGATE TO THE PARENT DIRECTORY OF WHERE THE JAR FILE IS LOCATED
CD \Java\Workspace\alphabet-soup

java -jar target\alphabet-soup-jar-with-dependencies.jar /old src/main/resources/input50x50.txt > c:\temp\old.txt
java -jar target\alphabet-soup-jar-with-dependencies.jar /old /async src/main/resources/input50x50.txt > c:\temp\oldsync.txt
java -jar target\alphabet-soup-jar-with-dependencies.jar src/main/resources/input50x50.txt > c:\temp\new.txt
java -jar target\alphabet-soup-jar-with-dependencies.jar /async src/main/resources/input50x50.txt > c:\temp\newsync.txt
pause
